var express = require('express');

const bodyParser= require('body-parser');
var cors = require('cors');
var app = express();
var db = require('./db');
var user = require('./user')
var deal = require('./deal')


app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json());

app.use(function (req, res, next){
  req.headers["content-type"] = "application/json"
     res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin,Authorization, X-Requested-With, Content-Type, Accept")
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  next()
})
app.use(cors());


app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
  console.log(req.body);
});

app.get('/deal',(req,res) =>{
	res.sendFile(__dirname + '/deal.html');
	console.log(req.body);
});


app.post('/users', user.createUsers);
app.get('/users', user.seeResults);
app.delete('/users/:id', user.delete)

app.post('/deal', deal.createDeal);
app.get('/deal',deal.seeDeal);


app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});


