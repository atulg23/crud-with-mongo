var mongoose = require('mongoose');  
var User = new mongoose.Schema({
	name: { type: String },
	age: Number,
  email: String
});

var Deal = new mongoose.Schema({
	address: {type: String},
	city: {type: String}
});


mongoose.model('User', User);  
mongoose.model('Deal', Deal);

mongoose.connect('mongodb://localhost/testing'); 

console.log('we are connected');
